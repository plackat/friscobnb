'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors'),
    Item = mongoose.model('Item'),
    request = require('request'),
    cheerio = require('cheerio'),
    moment = require('moment'),
    _ = require('lodash');

/**
 * Create a Item
 */
exports.create = function (req, res) {
    var item = new Item(req.body);
    item.user = req.user;

    item.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(item);
        }
    });
};

/**
 * Show the current Item
 */
exports.read = function (req, res) {
    var itemid = req.url.match(/\d+/)[0];
    var url = 'https://www.airbnb.com' + '/api/v1/listings/' + itemid + '/calendar?start_date=' + moment().format('YYYY-MM-DD') + '&end_date=' + moment().add(60, 'days').format('YYYY-MM-DD') + '&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=ILS&locale=en';
    request(url, function (error, resp, html) {
        req.dates = html;
        res.jsonp(req.dates);
    });

};

/**
 * Update a Item
 */
exports.update = function (req, res) {
    var item = req.item;

    item = _.extend(item, req.body);

    item.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(item);
        }
    });
};

/**
 * Delete an Item
 */
exports.delete = function (req, res) {
    var item = req.item;

    item.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(item);
        }
    });
};

/**
 * List of Items
 */
exports.list = function (req, res) {
    Item.find().sort('-created').populate('user', 'displayName').exec(function (err, items) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(items);
        }
    });
};

/**
 * Item middleware
 */
exports.itemByID = function (req, res, next, id) {
    Item.findById(id).populate('user', 'displayName').exec(function (err, item) {
        if (err) return next(err);
        if (!item) return next(new Error('Failed to load Item ' + id));
        req.item = item;
        next();
    });
};

/**
 * Item authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    if (req.item.user.id !== req.user.id) {
        return res.status(403).send('User is not authorized');
    }
    next();
};