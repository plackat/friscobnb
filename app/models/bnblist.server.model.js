
'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FriscoBnbSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        default: '',
        trim: true,
        required: 'Title cannot be blank'
    },
    price: {
        type: String,
        default: '',
        trim: true,
        required: 'Price cannot be blank'
    },
    reviews: {
        type: String,
        default: '0',
        trim: true
    },
    owner: {
        type: String,
        default: '',
        trim: true,
        required: 'Owner cannot be blank'
    },
    city: {
        type: String,
        default: '',
        trim: true,
        required: 'City cannot be blank'
    },
    viewcounter: {
        type: Number,
        default: 0,
        trim: true,
        required: 'Title cannot be blank'
    }
});

mongoose.model('FriscoBnb', FriscoBnbSchema);