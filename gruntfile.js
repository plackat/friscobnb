'use strict';

module.exports = function (grunt) {
    // Unified Watch Object
    var watchFiles = {
        serverViews: ['app/views/**/*.*'],
        serverJS: ['gruntfile.js', 'server.js', 'config/**/*.js', 'app/**/*.js'],
        clientViews: ['public/modules/**/views/**/*.html'],
        clientJS: ['public/js/*.js', 'public/modules/**/*.js'],
        clientCSS: ['public/modules/**/*.css'],
        mochaTests: ['app/tests/**/*.js']
    };

    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            serverViews: {
                files: watchFiles.serverViews,
                options: {
                    livereload: true
                }
            },
            serverJS: {
                files: watchFiles.serverJS,
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            clientViews: {
                files: watchFiles.clientViews,
                options: {
                    livereload: true,
                }
            },
            clientJS: {
                files: watchFiles.clientJS,
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            clientCSS: {
                files: watchFiles.clientCSS,
                tasks: ['csslint'],
                options: {
                    livereload: true
                }
            }
        },
        jshint: {
            all: {
                src: watchFiles.clientJS.concat(watchFiles.serverJS),
                options: {
                    jshintrc: true
                }
            }
        },
        csslint: {
            options: {
                csslintrc: '.csslintrc',
            },
            all: {
                src: watchFiles.clientCSS
            }
        },
        uglify: {
            production: {
                options: {
                    mangle: false
                },
                files: {
                    'public/dist/application.min.js': 'public/dist/application.js'
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'public/dist/application.min.css': '<%= applicationCSSFiles %>'
                }
            }
        },
        nodemon: {
            dev: {
                script: 'server.js',
                options: {
                    nodeArgs: ['--debug'],
                    ext: 'js,html',
                    watch: watchFiles.serverViews.concat(watchFiles.serverJS)
                }
            }
        },
        'node-inspector': {
            custom: {
                options: {
                    'web-port': 1337,
                    'web-host': 'localhost',
                    'debug-port': 5858,
                    'save-live-edit': true,
                    'no-preload': true,
                    'stack-trace-limit': 50,
                    'hidden': []
                }
            }
        },
        ngmin: {
            production: {
                files: {
                    'public/dist/application.js': '<%= applicationJavaScriptFiles %>'
                }
            }
        },
        concurrent: {
            default: ['nodemon', 'watch'],
            debug: ['nodemon', 'watch', 'node-inspector'],
            options: {
                logConcurrentOutput: true
            }
        },
        env: {
            test: {
                NODE_ENV: 'test'
            }
        },
        mochaTest: {
            src: watchFiles.mochaTests,
            options: {
                reporter: 'spec',
                require: 'server.js'
            }
        },
        karma: {
            unit: {
                configFile: 'karma.conf.js'
            }
        }
    });

    // Load NPM tasks
    require('load-grunt-tasks')(grunt);

    // Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    // A Task for loading the configuration object
    grunt.task.registerTask('loadConfig', 'Task that loads the config into a grunt option.', function () {
        var init = require('./config/init')();
        var config = require('./config/config');

        grunt.config.set('applicationJavaScriptFiles', config.assets.js);
        grunt.config.set('applicationCSSFiles', config.assets.css);
    });

    // Default task(s).
    grunt.registerTask('default', ['lint', 'concurrent:default']);

    // Debug task.
    grunt.registerTask('debug', ['lint', 'concurrent:debug']);

    // Lint task(s).
    grunt.registerTask('lint', ['jshint', 'csslint']);

    // Build task(s).
    grunt.registerTask('build', ['lint', 'loadConfig', 'ngmin', 'uglify', 'cssmin']);

    // Test task.
    grunt.registerTask('test', ['env:test', 'mochaTest', 'karma:unit']);

    grunt.registerTask('scrape', function () {
        //scraping task
        var request = require('request');
        var cheerio = require('cheerio');
        var Firebase = require('firebase');
        var myFirebaseRef = new Firebase('https://friskobnb.firebaseio.com/');
        var done = this.async();
        var scrapePage = function (html) {
            var $ = cheerio.load(html);
            $('div.listing').each(function (i, element) {
                var el = $(this);
                var currency = '', price = '',
                    pic = '', userpic = '',
                    userid = '', url = '',
                    itemdata = {}, moredata = '',
                    location = '', reviews = 0,
                    type = '';
                userpic = el.find('.media a[href*="/users"] img').attr('src');
                userid = el.find('.media a[href*="/users"]').attr('href').split('/')[3];
                url = el.find('.media a[href*="/rooms"]').attr('href');
                pic = el.find('.listing-img-container img').attr('src');
                price = el.find('.price-amount').text().trim();
                currency = el.find('.price-amount').prev('sup').text().trim();
                moredata = el.find('.listing-location').text().trim().split(/·|\&middot\;/);
                location = moredata[1].trim();
                type = moredata[0].trim();
                if (moredata.length === 3) {
                    reviews = moredata[1].trim().match(/\d+/)[0];
                    location = moredata[2].trim();
                }
                itemdata = {
                    title: el.attr('data-name'),
                    reviews: parseInt(reviews),
                    location: location,
                    type: type,
                    pic: pic,
                    url: url,
                    user: {pic: userpic, id: userid},
                    price: parseInt(price),
                    currency: currency,
                    name: parseInt(url.match(/\d+/)[0]),
                    counter:0
                };
                myFirebaseRef.push(itemdata);
            });
            setTimeout(done, 4000);
        };
        var fetchHTML = function (pagenumber, cb) {
            request('https://www.airbnb.com/s/94108?room_types%5B%5D=Entire+home%2Fapt&page=' + pagenumber, function(error, response, html) {
                if (!error && response.statusCode === 200) {
                    cb(html);
                }
            });
        };

        fetchHTML(1, function (html) {
                scrapePage(html);
                var $ = cheerio.load(html);
                var pagenumbers = [];
                var lastpage = $('.pagination ul li a');
                lastpage.each(function (i, el) {
                    pagenumbers.push($(el).attr('target'));
                });
                var numberofpages = Math.max.apply(Math, pagenumbers);
                for (var i = 2; i <= numberofpages; i++) {
                    fetchHTML(i, scrapePage);
                }

        });
    });

};