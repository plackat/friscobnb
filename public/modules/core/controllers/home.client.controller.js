'use strict';

angular.module('core').controller('HomeController', ['$scope', '$modal' , '$http', 'Authentication', '$filter', 'ngTableParams', '$firebase',
    function ($scope, $modal, $http, Authentication, $filter, ngTableParams, $firebase) {
        var ref = new Firebase("https://friskobnb.firebaseio.com/");
        $scope.list = $firebase(ref).$asArray();
        $scope.tableParams = new ngTableParams({
            page: 1,
            data: $scope.list,
            count: 150,
            sorting: {
                user: 'asc'
            }
        }, {
            total: $scope.list.length,
            getData: function ($defer, params) {
                var orderedData = params.sorting() ? $filter('orderBy')($scope.list, params.orderBy()) : $scope.list;
                $defer.resolve(orderedData);
            }
        });
        $scope.viewDates = function () {
            this.item.counter += 1;
            $scope.list.$save($scope.list.$indexFor(this.item.$id)).then(function() {});
            var res = $http.get(this.item.url,function (html) {
                console.log(arguments);
            }).success(function (res) {
                $scope.dates = JSON.parse(JSON.parse(res)).calendar.dates;
                $scope.dates = $scope.dates.filter(function (d) {
                    if (d.available) return true;
                });
                $scope.modalInstance = $modal.open({
                    templateUrl: 'myModalContent.html',
                    controller: function ($scope, $modalInstance, dates) {

                        $scope.dates = dates;
                        $scope.ok = function () {
                            $modalInstance.close($scope.selected.item);
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    },
                    resolve: {
                        dates: function () {
                            return $scope.dates;
                        }
                    }
                });
            });
        };
    }
]);